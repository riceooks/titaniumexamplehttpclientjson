//FirstView Component Constructor
function FirstView(params) {
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView();

	// UI
	var tableView = Ti.UI.createTableView();

	// HTTP Client XXX
	var jhr = Ti.Network.createHTTPClient({
		'onload' : function() {
			// Param
			var rowData = [];

			// Row data
			var json = JSON.parse(this.responseText);
			for (cat in json.categories) {
				var row = json.categories[cat];
				rowData.push({
					'title' : row.name,
					'cat_ID' : row.cat_ID,
					'color' : 'black'
				});
			}
			tableView.data = rowData;

			// Event
			tableView.addEventListener('click', function(event) {
				var winSub = Ti.UI.createWindow({
					'title' : event.rowData.title,
					'backgroundColor' : '#ffffff',
					'navBarHidden' : false,
					'exitOnClose' : false
				});

				// TableView
				var tableViewSub = Ti.UI.createTableView();

				// HTTP Client XXX
				var j2hr = Ti.Network.createHTTPClient({
					'onload' : function() {
						// Param
						var row2Data = [];

						// Row data
						var json2 = JSON.parse(this.responseText);
						for (p in json2.posts) {
							var row = json2.posts[p];
							row2Data.push({
								'title' : row.post_title,
								'content' : row.post_content,
								'color' : 'black'
							});
						}
						tableViewSub.data = row2Data;

						// Event
						tableViewSub.addEventListener('click', function(event2) {
							var winContent = Ti.UI.createWindow({
								'title' : event2.rowData.title,
								'navBarHidden' : false,
								'exitOnClose' : false
							});

							var webView = Ti.UI.createWebView({
								'html' : event2.rowData.content
							});

							winContent.add(webView);

							if (Ti.Platform.osname == 'iphone') {
								params.nav.open(winContent, {
									'animated' : true
								});
							} else {
								winContent.open({
									'animated' : true
								});
							}
						});

						winSub.add(tableViewSub);
					}
				})
				j2hr.open('GET', 'http://192.168.1.103/barryblog/barry-json.php?action=posts&cat_id=' + event.rowData.cat_ID);
				j2hr.send();

				if (Ti.Platform.osname == 'iphone') {
					params.nav.open(winSub, {
						'animated' : true
					});
				} else {
					winSub.open({
						'animated' : true
					});
				}
			});

			self.add(tableView);
		}
	});

	jhr.open('GET', 'http://192.168.1.103/barryblog/barry-json.php?action=categories');
	jhr.send();

	return self;
}

module.exports = FirstView;
