//Application Window Component Constructor
function ApplicationWindow() {
	//load component dependencies
	var FirstView = require('ui/common/FirstView');

	//create component instance
	var self = Ti.UI.createWindow({
		backgroundColor : '#ffffff'
	});

	// UI
	var winBody = Ti.UI.createWindow({
		'title' : 'BarryBlog',
		'backgroundColor' : 'white'
	});

	// Nav
	var nav = Ti.UI.iPhone.createNavigationGroup({
		'window' : winBody
	});
	self.add(nav);

	//construct UI
	var firstView = new FirstView({
		'nav' : nav
	});

	winBody.add(firstView);

	return self;
}

//make constructor function the public component interface
module.exports = ApplicationWindow;
